
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
  <head>
    <meta name="layout" content="main" />
    <title>Audit Log</title>
  </head>
  <body>
  <h1 class="text-center">Audit Log</h1>

  <hr />

<center>
        <div class="table-responsive">
            <table class="table table-condensed table-hover table-striped table-bordered">
              <thead>
                <tr>
                  <td><strong>Date/Time Action Executed</strong></td>
                  <td><strong>Action Executed</strong></td>
                  <td><strong>User Executed</strong></td>
                </tr>
              </thead>
              <tbody>
                <g:each in="${auditLogInstanceList}" var="auditLogInstance">
                  <tr>
                    <td>${auditLogInstance.actionTime}</td>
                    <td>${auditLogInstance.action}</td>
                    <td>${auditLogInstance.user}</td>
                  </tr>
                </g:each>
              </tbody>
            </table>
          </div>
</center>


  </body>
</html>