<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="Grails"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" href="${assetPath(src: 'favicon.png')}" type="image/x-icon">
		<link rel="apple-touch-icon" href="${assetPath(src: 'favicon.png')}">
		<link rel="apple-touch-icon" sizes="114x114" href="${assetPath(src: 'favicon.png')}">
  		<asset:stylesheet src="application.css"/>
		<asset:javascript src="application.js"/>
		<g:layoutHead/>
	</head>
	<body>
		<sec:ifLoggedIn>
			<div id="header">
				<div>
					<g:link uri="/"><asset:image src="logo.png" style="width: 100px; padding-bottom: 30px;" /></g:link>
						<ul class="nav nav-pills pull-right">
							<li role="presentation" class="active"><g:link uri="/">Home</g:link></li>
							<li role="presentation"><g:link controller="profile" action="edit">Profile</g:link></li>
							<li role="presentation"><g:link controller="logout" action="index">Log Out</g:link></li>
						</ul>
				</div>
			</div>
		</sec:ifLoggedIn>
		<sec:ifNotLoggedIn>
			<div id="small-header">
				<div>
					<g:link uri="/"><asset:image src="logo.png" style="width: 100px;" /></g:link>
					<ul class="nav nav-pills pull-right">
						<li role="presentation" class="active"><g:link uri="/">Home</g:link></li>
						<li role="presentation"><g:link controller="login" action="registration">Register</g:link></li>
						<li role="presentation"><g:link controller="login" action="auth">Sign In</g:link></li>
					</ul>
				</div>
			</div>
		</sec:ifNotLoggedIn>
		<div id="contents">
			<div id="tagline" class="clearfix">

				<g:layoutBody/>

			</div>
		</div>
		<div id="footer">
			<div class="clearfix">
				<div id="connect">
					<a href="http://facebook.com" target="_blank" class="facebook"></a><a href="http://twitter.com" target="_blank" class="twitter"></a>
				</div>
				<p>
					© ${new Date().format("yyyy")} Brandon Wagner and Dan Fujita. All Rights Reserved.
				</p>
			</div>
		</div>
	</body>
</html>
