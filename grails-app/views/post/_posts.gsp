<%@ page import="com.edupal.security.SecUser" %>
<span id="post-update">
    <div class="row">
        <div class="col-md-offset-3 col-md-6">
            <g:form class="form-horizontal" controller="post">
                <center>
                    <g:textArea name="postBox" placeholder="Type Post Here" />
                </center>
                <g:submitToRemote class="form-control" update="post-update" controller="post" action="addPost" value="Add Post" name="submitPost" />
            </g:form>
        </div>
    </div>
    <br />
    <g:each in="${com.edupal.Post.getPosts(com.edupal.security.SecUser.load(sec.loggedInUserInfo(field: "id")))}" var="post">
        <div class="row">
            <div class="col-md-offset-2 col-md-8">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">${post.user} at ${post.postDate.format("MM-dd-yyyy HH:mm")}</h3>
                    </div>
                    <div class="panel-body">
                        ${post}
                    </div>
                </div>
            </div>
        </div>
    </g:each>
</span>