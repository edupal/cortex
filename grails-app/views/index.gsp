
<%@ page contentType="text/html;charset=UTF-8" %>
<html>
    <head>
        <title>Cortex</title>
        <meta name="layout" content="main">
    </head>

    <body>
        <sec:ifNotLoggedIn>
            <h1>Cortex ...</h1>
            <p class="lead">
                Cortex is a next generation data analytics platform for education. Cortex will redefine analytics for students, faculty, and administrators at education institutions.
                Cortex utilizes intelligent machine learning algorithms to perform complex analytics on your educational data and helps to inform you and your institution of areas to improve in.
                Cortex also incorporates a social network where students can help to fill in data into the system and run analytics on themselves.  This enables students to get self-advising and engage in path
                 simulations with an unbiased system.
            </p>
            <br />
            <h1>Getting Started ...</h1>
            <p class="lead">
                Jumping into Cortex is easy! Simply <g:link controller="login" action="registration">register</g:link> with your .edu email address and Cortex will automatically link your account with your instituion.
                If we have data on you already provided by your instituion, we will link it to your account and give you your immediate analysis. You can also work on your profile. You profile
                will allow you to set goals and give a current bio and some general information about yourself. This helps us to personalize your analytics.
            </p>
            <br />

            <h1>Institutions Involved Already ...</h1>
            <p>
                <asset:image src="GMU_logo.png" style="height: 100px"/>
            </p>
        </sec:ifNotLoggedIn>
        <sec:ifLoggedIn>
            <g:form class="form-horizontal form-inline" controller="profile">
                Search:
                <g:select name="profileSearch" from="${com.edupal.Student.all}" class="select2" optionKey="id" value=""/>
                <g:actionSubmit action="goToProfile" name="searchButton" class="" value="Go" />
            </g:form>

            <g:render template="/post/posts" />



            <script>
                $('#profileSearch').select2();
            </script>
        </sec:ifLoggedIn>


    </body>
</html>