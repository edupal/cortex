
<%@ page import="com.edupal.Student; com.edupal.security.SecUser; com.edupal.security.LastLogin" contentType="text/html;charset=UTF-8" %>
<html>
<head>
  <title>Edit Student Profile</title>
  <meta name="layout" content="main" />
</head>
<body>

  <g:set var="editFlag" value="${com.edupal.Student.findByUser(com.edupal.security.SecUser.get(sec.loggedInUserInfo(field: 'id'))) == student}" />

  <h1 class="text-center"><g:fieldValue field="name" bean="${student}" /></h1>
  <p class="lead">
    <g:fieldValue field="name" bean="${student}" /> | Last Login: ${lastLogin}
  </p>

  <hr />
    <p class="lead">
      <g:render template="templates/bio" />
    </p>
  <hr />
  <br />
  <h2>Academic: </h2>

  <form class="form-horizontal">
    <div class="form-group">
      <label class="col-sm-2 control-label">University: </label>
      <g:render template="templates/university" model="[editable: editFlag]" />
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Majors: </label>
      <g:render template="templates/majors" model="[editable: editFlag]" />
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Minors: </label>
      <g:render template="templates/minors" model="[editable: editFlag]" />
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Organizations: </label>
      <div class="col-sm-10">
        <g:render template="templates/organizations" model="[editable: editFlag]" />
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">This Semester Courses: </label>
      <div class="col-sm-10">
        <g:render template="templates/courses" model="[editable: editFlag]" />
      </div>
    </div>
  </form>

<hr />

<h2>Attributes: </h2>

%{--This should iterate through the students attribute values. --}%
%{--They should also be able to add custom attribute values or select from a list of attribute values other students have added.--}%

<form class="form-horizontal">
  <g:each in="${student?.attributes}" var="attributeVal">
    <div class="form-group">
      <label class="col-sm-2 control-label">${attributeVal?.attribute?.name} </label>
      <div class="col-sm-10">
        <p class="form-control-static">${attributeVal?.value}</p>
      </div>
    </div>
  </g:each>

</form>


<hr />
<h2>Comments</h2>

  <span id="comment-update">
      <g:render template="templates/comments" />
  </span>





</body>
</html>