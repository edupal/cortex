<script>
    $('.select2').select2();
</script>
<span id="major-edit">
    <g:form class="form-inline">
        <div class="form-group">
            <g:select from="${com.edupal.Major.all}" name="major" optionKey="id" value="${student?.majors*.id}" class="form-control select2" placeholder="Major" />
            <g:submitToRemote class="form-control" update="major-edit" action="updateMajor" value="Save" name="submitMajorEdit" />
        </div>
    </g:form>

</span>