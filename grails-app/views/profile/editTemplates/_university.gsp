<script>
    $('.select2').select2();
</script>
<span id="university-edit">
  <g:form class="form-inline">
      <div class="form-group">
        <g:select from="${com.edupal.University.all}" name="university" optionKey="id" value="${student?.university?.id}" class="form-control select2" placeholder="New University" />
        <g:submitToRemote class="form-control" update="university-edit" action="updateUniversity" value="Save" name="submitUniversityEdit" />
      </div>
  </g:form>

</span>