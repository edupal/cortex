<span id="bio-edit">
    <g:form class="form-inline">
        <div class="form-group">
            <g:textArea name="bio" class="form-control text-area" placeholder="Bio" />
            <g:submitToRemote class="form-control" update="bio-edit" action="updateBio" value="Save" name="submitBioEdit" />
        </div>
        <div class="help">250 Character Max</div>
    </g:form>

</span>