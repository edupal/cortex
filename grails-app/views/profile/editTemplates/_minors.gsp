<script>
    $('.select2').select2();
</script>
<span id="minor-edit">
    <g:form class="form-inline">
        <div class="form-group">
            <g:select from="${com.edupal.Minor.all}" name="minor" optionKey="id" value="${student?.minors*.id}" class="form-control select2" placeholder="Minor" />
            <g:submitToRemote class="form-control" update="minor-edit" action="updateMinor" value="Save" name="submitMinorEdit" />
        </div>
    </g:form>

</span>