<script>
  $('.select2').select2();
</script>
<span id="courses-edit">
  <g:form class="form-inline">
    <div class="form-group">
      <g:select from="${com.edupal.Course.all}" name="courses" optionKey="id" value="${student?.courses*.id}" class="form-control select2" placeholder="Course" />
      <g:submitToRemote class="form-control" update="courses-edit" action="updateCourses" value="Save" name="submitCourseEdit" />
    </div>
  </g:form>

</span>