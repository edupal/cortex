<%@ page import="com.edupal.Student; com.edupal.security.SecUser" %>
<script>
    $('.select2').select2();
</script>
<span id="organizations-edit">
    <g:form class="form-inline">
        <div class="form-group">
            <g:select from="${organizations}" name="organizations" optionKey="id" value="${student?.organizations*.id}" class="form-control select2" placeholder="Organization" />
            <g:submitToRemote class="form-control" update="organizations-edit" action="updateOrganizations" value="Save" name="submitOrganizationEdit" />
        </div>
    </g:form>

</span>