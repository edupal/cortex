<div class="row">
    <div class="col-md-offset-3 col-md-6">
        <g:form class="form-horizontal">
            <center>
                <g:textArea name="commentBox" placeholder="Type Comment Here" />
            </center>
            <g:submitToRemote class="form-control" update="comment-update" action="addComment" value="Add Comment" name="submitComment" />
        </g:form>
    </div>
</div>
<br />
<g:each in="${student.comments.sort{it.commentDate}.reverse()}" var="comment">
    <div class="row">
        <div class="col-md-offset-3 col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">${comment.user} at ${comment.commentDate.format("MM-dd-yyyy HH:mm")}</h3>
                </div>
                <div class="panel-body">
                    ${comment}
                </div>
            </div>
        </div>
    </div>
</g:each>