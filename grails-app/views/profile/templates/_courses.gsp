<div class="col-sm-10">
    <span id="courses-show">
        <p class="form-control-static">${student?.displayCourses()} <g:if test="${editable}"><g:remoteLink action="editCourses" update="courses-show"><span class="glyphicon glyphicon-pencil"></span></g:remoteLink></g:if></p>
    </span>
</div>