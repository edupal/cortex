<div class="col-sm-10">
    <span id="major-show">
        <p class="form-control-static">${student?.displayMajors()} <g:if test="${editable}"><g:remoteLink action="editMajor" update="major-show"><span class="glyphicon glyphicon-pencil"></span></g:remoteLink></g:if></p>
    </span>
</div>