    <span id="bio-show">
        ${student?.bio ?: "Bio: "} <g:if test="${editable}"> <g:remoteLink action="editBio" update="bio-show"><span class="glyphicon glyphicon-pencil"></span></g:remoteLink></g:if>
    </span>
