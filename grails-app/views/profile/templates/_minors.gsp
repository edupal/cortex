<div class="col-sm-10">
    <span id="minor-show">
        <p class="form-control-static">${student?.displayMinors()} <g:if test="${editable}"><g:remoteLink action="editMinor" update="minor-show"><span class="glyphicon glyphicon-pencil"></span></g:remoteLink></g:if></p>
    </span>
</div>