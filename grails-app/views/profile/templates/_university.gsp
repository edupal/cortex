<div class="col-sm-10">
    <span id="university-show">
        <p class="form-control-static"><g:fieldValue field="university" bean="${student}" /> <g:if test="${editable}"><g:remoteLink action="editUniversity" update="university-show"><span class="glyphicon glyphicon-pencil"></span></g:remoteLink></g:if></p>
    </span>
</div>