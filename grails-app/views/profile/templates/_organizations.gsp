<div class="col-sm-10">
    <span id="organizations-show">
        <p class="form-control-static">${student?.displayOrganizations()} <g:if test="${editable}"><g:remoteLink action="editOrganizations" update="organizations-show"><span class="glyphicon glyphicon-pencil"></span></g:remoteLink></g:if></p>
    </span>
</div>