<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <script src="https://www.google.com/recaptcha/api.js?onload=loadCaptcha&render=explicit" async defer></script>    <meta name="layout" content="main"/>
    <script type='text/javascript'>
        var captchaContainer = null;
        var loadCaptcha = function() {
            captchaContainer = grecaptcha.render('captcha_container', {
                'sitekey' : '6Lf85wUTAAAAAFj5G7kPyNFaZ96yDcHprOUjIXZO',
                'callback' : function(response) {
                    console.log(response);
                }
            });
        };
    </script>
  <title>Registration</title>
</head>
<body>

    <h1 class="text-center">Registration</h1>
<br />

  <g:form>

      <div class="form-group">
        <label class="reglabel" for="fullname">Full Name</label>
        <input type="text" class="form-control input-lg" id="fullname" name="fullname" placeholder="First and Last Name">
      </div>

      <div class="form-group">
        <label class="reglabel" for="username">Username</label>
        <input type="text" class="form-control input-lg" id="username" name="username" placeholder="Pick a Username">
      </div>

      <div class="form-group">
        <label class="reglabel" for="email">Email address</label>
        <input type="email" class="form-control input-lg" id="email" name="email" placeholder="Enter Email">
      </div>

      <div class="form-group">
        <label class="reglabel" for="password">Password</label>
        <input type="password" class="form-control input-lg" id="password" name="password" placeholder="Password">
      </div>

      <div class="form-group">
        <label class="reglabel" for="confirmPassword">Confirm Password</label>
        <input type="password" class="form-control input-lg" id="confirmPassword" name="confirmPassword" placeholder="Confirm Password">
      </div>

      <div id="captcha_container"></div>

      <p></p>
      <g:actionSubmit value="Submit" class="btn btn-default center-block" action="register" />

  </g:form>

</body>
</html>