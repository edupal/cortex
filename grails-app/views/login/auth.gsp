<html>
<head>
	<title>Cortex Login</title>
	<meta name="layout" content="main"/>
</head>
<body>

		<form action='${postUrl}' method='POST' autocomplete='off' class="text-center center-block">

			<H2>Sign in</H2>
			<input name="j_username" class="form-control input-lg" id="username" size="20" placeholder="Email or Username"/>
			<br />
			<input type="password" class="form-control input-lg" name="j_password" id="password" size="20" placeholder="Password" />
			<br />
			<g:link controller='register' action='forgotPassword'>Forgot Password?</g:link>
			<br />
			<button type="submit" name="mybutton" id="signin" class="btn btn-default">Sign in</button>
		</form>
<script>
$(document).ready(function() {
	$('#username').focus();
});

</script>
</body>
</html>
