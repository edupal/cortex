package com.edupal


class AuditFilterFilters {

    def auditService

    def filters = {
        all(controller:'profile|post', action:'*') {
            before = {

            }
            after = { Map model ->

            }
            afterView = { Exception e ->
                auditService.recordAudit(controllerName, actionName)
            }
        }
    }
}
