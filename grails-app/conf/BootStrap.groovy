
class BootStrap {

    def securityStartupService
    def dataProviderService

    def init = { servletContext ->

        securityStartupService.initData()

        dataProviderService.initData()


    }
    def destroy = {
    }

}
