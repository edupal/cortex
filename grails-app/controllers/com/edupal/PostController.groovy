package com.edupal

import com.edupal.security.SecUser
import grails.transaction.Transactional

@Transactional
class PostController {

    def springSecurityService

    def addPost() {
        Post post = new Post(post: params.postBox, user: SecUser.get(springSecurityService.principal.id)).save(failOnError: true, flush: true)
        render(template: '/post/posts')
    }
}
