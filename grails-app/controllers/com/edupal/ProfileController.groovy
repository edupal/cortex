package com.edupal

import com.edupal.security.LastLogin
import com.edupal.security.SecUser
import grails.transaction.Transactional

@Transactional
class ProfileController {

    def springSecurityService

    def index() {
    }

    def goToProfile(){
        redirect(action: 'profile', id: params.profileSearch)
    }

    def edit(){
        Student student = Student.findByUser(SecUser.load(springSecurityService.getPrincipal().id))
        def lastLogins
        try {
            lastLogins = LastLogin.findAllByUser(student.user, [max: 2, sort: "lastLogin", order: "desc"])
        }catch(Exception e){
        }
        LastLogin lastLogin
        if(lastLogins?.size() > 1){
            lastLogin = lastLogins[1]
        }
        else if(lastLogins?.size() == 1) {
            lastLogin = lastLogins[0]
        }

        [student: student, lastLogin: lastLogin]
    }

    def profile(Long id){
        [student: Student.load(id), editable: false]
    }

    def addComment(){
        SecUser user = SecUser.load(springSecurityService.getPrincipal().id)
        Student student  = Student.findByUser(user)
        Comment comment = new Comment(user:user, comment: params.commentBox).save(flush:true, failOnError: true)
        student.addToComments(comment).save(flush:true)
        render(template: 'templates/comments', model: [student: student])
    }

    def editUniversity(){
        SecUser user = SecUser.load(springSecurityService.getPrincipal().id)
        Student student  = Student.findByUser(user)
        render template: "/profile/editTemplates/university", model: [student: Student.findByUser(SecUser.load(springSecurityService.getPrincipal().id))]
    }

    def updateBio(){
        Student student = Student.findByUser(SecUser.load(springSecurityService.getPrincipal().id))
        student.bio = params.bio
        student.save(flush:true)
        render(template: "/profile/templates/bio", model: [student: student])
    }

    def editBio(){
        render template: "/profile/editTemplates/bio", model: [student: Student.findByUser(SecUser.load(springSecurityService.getPrincipal().id))]
    }

    def updateUniversity(){
        Student student = Student.findByUser(SecUser.load(springSecurityService.getPrincipal().id))
        University updatedUniversity = University.load(params.university)
        if(student.university != updatedUniversity){
            student.majors = null
            student.minors = null
            student.courses = null
            student.organizations = null
            student.university = updatedUniversity
            student.save(flush: true)
        }
        render(template: "/profile/templates/university", model: [student: student])
    }

    def editMajor(){
        render template: "/profile/editTemplates/majors", model: [student: Student.findByUser(SecUser.load(springSecurityService.getPrincipal().id))]
    }

    def updateMajor(){
        Student student = Student.findByUser(SecUser.load(springSecurityService.getPrincipal().id))
        if(params.major instanceof String){
            params.major = [params.major]
        }
        Major major
        params.major.each{
            major = Major.load(it)
            if(!student.majors.contains(major))
                student.addToMajors(major)
        }
        student.save(flush: true)
        render(template: "/profile/templates/majors", model: [student: student])
    }

    def editMinor(){
        render template: "/profile/editTemplates/minors", model: [student: Student.findByUser(SecUser.load(springSecurityService.getPrincipal().id))]
    }

    def updateMinor(){
        Student student = Student.findByUser(SecUser.load(springSecurityService.getPrincipal().id))
        if(params.minor instanceof String){
            params.minor = [params.minor]
        }
        Minor minor
        params.minor.each{
            minor = Minor.load(it)
            if(!student.minors.contains(minor))
                student.addToMinors(minor)
        }
        student.save(flush: true)
        render(template: "/profile/templates/minors", model: [student: student])
    }

    def editOrganizations(){
        Student student = Student.findByUser(SecUser.load(springSecurityService.getPrincipal().id))
        def organizations = Organization.findAllByUniversity(student.university)
        render template: "/profile/editTemplates/organizations", model: [organizations: organizations, student: student]
    }

    def updateOrganizations(){
        Student student = Student.findByUser(SecUser.load(springSecurityService.getPrincipal().id))
        if(params.organizations instanceof String){
            params.organizations = [params.organizations]
        }
        Organization organization
        params.organizations.each{
            organization = Organization.load(it)
            if(!student.organizations.contains(organization))
                student.addToOrganizations(organization)
        }
        student.save(flush: true)
        render(template: "/profile/templates/organizations", model: [student: student])
    }

    def editCourses(){
        render template: "/profile/editTemplates/courses", model: [student: Student.findByUser(SecUser.load(springSecurityService.getPrincipal().id))]
    }

    def updateCourses(){
        Student student = Student.findByUser(SecUser.load(springSecurityService.getPrincipal().id))
        if(params.courses instanceof String){
            params.courses = [params.courses]
        }
        Course course
        params.courses.each{
            course = Course.load(it)
            if(!student.courses.contains(course))
                student.addToCourses(course)
        }
        student.save(flush: true)
        render(template: "/profile/templates/courses", model: [student: student])
    }
}
