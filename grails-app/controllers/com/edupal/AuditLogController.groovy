package com.edupal

class AuditLogController {

    def index() {
        [auditLogInstanceList: AuditLog.all.sort{it.actionTime}]
    }
}
