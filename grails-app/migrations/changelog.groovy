databaseChangeLog = {

	changeSet(author: "brandon (generated)", id: "1428871464640-1") {
		createTable(tableName: "attribute") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "attributePK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "category", type: "varchar(255)") {
				constraints(nullable: "false")
			}

			column(name: "name", type: "varchar(255)") {
				constraints(nullable: "false")
			}

			column(name: "weight", type: "decimal(19,2)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-2") {
		createTable(tableName: "attribute_values") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "attribute_valPK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "attribute_id", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "value", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-3") {
		createTable(tableName: "college") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "collegePK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "name", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-4") {
		createTable(tableName: "college_department") {
			column(name: "college_departments_id", type: "bigint")

			column(name: "department_id", type: "bigint")
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-5") {
		createTable(tableName: "course") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "coursePK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "name", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-6") {
		createTable(tableName: "department") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "departmentPK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "name", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-7") {
		createTable(tableName: "department_course") {
			column(name: "department_courses_id", type: "bigint")

			column(name: "course_id", type: "bigint")
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-8") {
		createTable(tableName: "department_major") {
			column(name: "department_majors_id", type: "bigint")

			column(name: "major_id", type: "bigint")
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-9") {
		createTable(tableName: "department_minor") {
			column(name: "department_minors_id", type: "bigint")

			column(name: "minor_id", type: "bigint")
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-10") {
		createTable(tableName: "knowledge_base") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "knowledge_basPK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-11") {
		createTable(tableName: "knowledge_base_attribute") {
			column(name: "knowledge_base_attributes_id", type: "bigint")

			column(name: "attribute_id", type: "bigint")
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-12") {
		createTable(tableName: "major") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "majorPK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "name", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-13") {
		createTable(tableName: "major_course") {
			column(name: "major_courses_id", type: "bigint")

			column(name: "course_id", type: "bigint")
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-14") {
		createTable(tableName: "minor") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "minorPK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "name", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-15") {
		createTable(tableName: "minor_course") {
			column(name: "minor_courses_id", type: "bigint")

			column(name: "course_id", type: "bigint")
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-16") {
		createTable(tableName: "organization") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "organizationPK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "name", type: "varchar(255)") {
				constraints(nullable: "false")
			}

			column(name: "university_id", type: "bigint") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-17") {
		createTable(tableName: "right_group") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "right_groupPK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "name", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-18") {
		createTable(tableName: "right_group_sec_right") {
			column(name: "right_group_id", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "sec_right_id", type: "bigint") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-19") {
		createTable(tableName: "sec_right") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "sec_rightPK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "authority", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-20") {
		createTable(tableName: "sec_user") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "sec_userPK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "account_expired", type: "boolean") {
				constraints(nullable: "false")
			}

			column(name: "account_locked", type: "boolean") {
				constraints(nullable: "false")
			}

			column(name: "enabled", type: "boolean") {
				constraints(nullable: "false")
			}

			column(name: "password", type: "varchar(255)") {
				constraints(nullable: "false")
			}

			column(name: "password_expired", type: "boolean") {
				constraints(nullable: "false")
			}

			column(name: "username", type: "varchar(64)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-21") {
		createTable(tableName: "sec_user_right_group") {
			column(name: "right_group_id", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "sec_user_id", type: "bigint") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-22") {
		createTable(tableName: "sec_user_sec_right") {
			column(name: "sec_right_id", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "sec_user_id", type: "bigint") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-23") {
		createTable(tableName: "student") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "studentPK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "name", type: "varchar(255)") {
				constraints(nullable: "false")
			}

			column(name: "probability_of_success", type: "decimal(19,2)")

			column(name: "status", type: "varchar(255)")

			column(name: "university_id", type: "bigint")

			column(name: "user_id", type: "bigint") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-24") {
		createTable(tableName: "student_attribute") {
			column(name: "student_attributes_id", type: "bigint")

			column(name: "attribute_id", type: "bigint")
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-25") {
		createTable(tableName: "student_course") {
			column(name: "student_courses_id", type: "bigint")

			column(name: "course_id", type: "bigint")
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-26") {
		createTable(tableName: "student_major") {
			column(name: "student_majors_id", type: "bigint")

			column(name: "major_id", type: "bigint")
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-27") {
		createTable(tableName: "student_minor") {
			column(name: "student_minors_id", type: "bigint")

			column(name: "minor_id", type: "bigint")
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-28") {
		createTable(tableName: "student_organization") {
			column(name: "student_organizations_id", type: "bigint")

			column(name: "organization_id", type: "bigint")
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-29") {
		createTable(tableName: "university") {
			column(autoIncrement: "true", name: "id", type: "bigint") {
				constraints(nullable: "false", primaryKey: "true", primaryKeyName: "universityPK")
			}

			column(name: "version", type: "bigint") {
				constraints(nullable: "false")
			}

			column(name: "name", type: "varchar(255)") {
				constraints(nullable: "false")
			}
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-30") {
		createTable(tableName: "university_college") {
			column(name: "university_colleges_id", type: "bigint")

			column(name: "college_id", type: "bigint")
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-31") {
		addPrimaryKey(columnNames: "right_group_id, sec_right_id", constraintName: "right_group_sPK", tableName: "right_group_sec_right")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-32") {
		addPrimaryKey(columnNames: "right_group_id, sec_user_id", constraintName: "sec_user_righPK", tableName: "sec_user_right_group")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-33") {
		addPrimaryKey(columnNames: "sec_right_id, sec_user_id", constraintName: "sec_user_sec_PK", tableName: "sec_user_sec_right")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-70") {
		createIndex(indexName: "UK_5gfb3sl3v66b5mmv5p2fs6365", tableName: "right_group", unique: "true") {
			column(name: "name")
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-71") {
		createIndex(indexName: "name_uniq_1428871464573", tableName: "right_group", unique: "true") {
			column(name: "name")
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-72") {
		createIndex(indexName: "UK_s6sqnkry4gpvlnc934aal5s4u", tableName: "sec_right", unique: "true") {
			column(name: "authority")
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-73") {
		createIndex(indexName: "authority_uniq_1428871464580", tableName: "sec_right", unique: "true") {
			column(name: "authority")
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-74") {
		createIndex(indexName: "UK_5ctbdrlf3eismye20vsdtk8w8", tableName: "sec_user", unique: "true") {
			column(name: "username")
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-75") {
		createIndex(indexName: "username_uniq_1428871464581", tableName: "sec_user", unique: "true") {
			column(name: "username")
		}
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-34") {
		addForeignKeyConstraint(baseColumnNames: "attribute_id", baseTableName: "attribute_values", constraintName: "FK_q6j4aaombbuhncyivjcm5a0vs", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "attribute", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-35") {
		addForeignKeyConstraint(baseColumnNames: "college_departments_id", baseTableName: "college_department", constraintName: "FK_2qri9try78cuhvs1c90dy8149", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "college", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-36") {
		addForeignKeyConstraint(baseColumnNames: "department_id", baseTableName: "college_department", constraintName: "FK_5ypy1yq677iu4w9nobc6bk3wl", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "department", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-37") {
		addForeignKeyConstraint(baseColumnNames: "course_id", baseTableName: "department_course", constraintName: "FK_ltpisex4u20r6hqsipmhpyryh", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "course", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-38") {
		addForeignKeyConstraint(baseColumnNames: "department_courses_id", baseTableName: "department_course", constraintName: "FK_osxn3ijrl2u9lrivsvml9dv87", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "department", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-39") {
		addForeignKeyConstraint(baseColumnNames: "department_majors_id", baseTableName: "department_major", constraintName: "FK_sd3dmaj4gvxt2o19gprm1en36", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "department", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-40") {
		addForeignKeyConstraint(baseColumnNames: "major_id", baseTableName: "department_major", constraintName: "FK_lp778b2t4b4bgp5afj91i6eks", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "major", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-41") {
		addForeignKeyConstraint(baseColumnNames: "department_minors_id", baseTableName: "department_minor", constraintName: "FK_4jw82gw33k0ajuag4br2tje6e", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "department", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-42") {
		addForeignKeyConstraint(baseColumnNames: "minor_id", baseTableName: "department_minor", constraintName: "FK_741yh6adtq9eb3qwyfqmayawd", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "minor", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-43") {
		addForeignKeyConstraint(baseColumnNames: "attribute_id", baseTableName: "knowledge_base_attribute", constraintName: "FK_31m7p35arh5c6xfambnby019y", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "attribute", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-44") {
		addForeignKeyConstraint(baseColumnNames: "knowledge_base_attributes_id", baseTableName: "knowledge_base_attribute", constraintName: "FK_fcguya0mukoqt7rl3vumfvy39", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "knowledge_base", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-45") {
		addForeignKeyConstraint(baseColumnNames: "course_id", baseTableName: "major_course", constraintName: "FK_sxy3p4sy9msdpe2xfljh722cy", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "course", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-46") {
		addForeignKeyConstraint(baseColumnNames: "major_courses_id", baseTableName: "major_course", constraintName: "FK_repjxgeo37opbkrexh9lh8fj", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "major", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-47") {
		addForeignKeyConstraint(baseColumnNames: "course_id", baseTableName: "minor_course", constraintName: "FK_9j6wd167m3cap11kbrfbdl5or", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "course", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-48") {
		addForeignKeyConstraint(baseColumnNames: "minor_courses_id", baseTableName: "minor_course", constraintName: "FK_bdxcx44htb92adox4ihj7elci", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "minor", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-49") {
		addForeignKeyConstraint(baseColumnNames: "university_id", baseTableName: "organization", constraintName: "FK_hllq2shm5uulj54bwti48ux1v", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "university", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-50") {
		addForeignKeyConstraint(baseColumnNames: "right_group_id", baseTableName: "right_group_sec_right", constraintName: "FK_p17vc23lx69yuldiix2ryib3", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "right_group", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-51") {
		addForeignKeyConstraint(baseColumnNames: "sec_right_id", baseTableName: "right_group_sec_right", constraintName: "FK_oon1m5p30ttj2dceopn26a8wi", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "sec_right", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-52") {
		addForeignKeyConstraint(baseColumnNames: "right_group_id", baseTableName: "sec_user_right_group", constraintName: "FK_ddgxm600j2nm6ayr7mj8fe3qf", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "right_group", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-53") {
		addForeignKeyConstraint(baseColumnNames: "sec_user_id", baseTableName: "sec_user_right_group", constraintName: "FK_4ttrxctjbx7k9ai2vps3m28rj", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "sec_user", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-54") {
		addForeignKeyConstraint(baseColumnNames: "sec_right_id", baseTableName: "sec_user_sec_right", constraintName: "FK_fqxu2vc8mwm3keet2vc7co3ao", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "sec_right", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-55") {
		addForeignKeyConstraint(baseColumnNames: "sec_user_id", baseTableName: "sec_user_sec_right", constraintName: "FK_sqgepfk36rx4mhpsqe7lcdd5m", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "sec_user", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-56") {
		addForeignKeyConstraint(baseColumnNames: "university_id", baseTableName: "student", constraintName: "FK_a9nmvxoolj4das3ghh758un56", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "university", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-57") {
		addForeignKeyConstraint(baseColumnNames: "user_id", baseTableName: "student", constraintName: "FK_bkix9btnoi1n917ll7bplkvg5", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "sec_user", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-58") {
		addForeignKeyConstraint(baseColumnNames: "attribute_id", baseTableName: "student_attribute", constraintName: "FK_d2p23xnac5ixbuincu3boqn4x", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "attribute", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-59") {
		addForeignKeyConstraint(baseColumnNames: "student_attributes_id", baseTableName: "student_attribute", constraintName: "FK_1w9h8gt0smif9qykfd2jhjmfb", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "student", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-60") {
		addForeignKeyConstraint(baseColumnNames: "course_id", baseTableName: "student_course", constraintName: "FK_8eu2c4tg0i8amuwx6n6d4i8h0", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "course", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-61") {
		addForeignKeyConstraint(baseColumnNames: "student_courses_id", baseTableName: "student_course", constraintName: "FK_m0vumwusgno50koccxun0olfr", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "student", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-62") {
		addForeignKeyConstraint(baseColumnNames: "major_id", baseTableName: "student_major", constraintName: "FK_4pkteny6rsg4774qygp7ibahs", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "major", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-63") {
		addForeignKeyConstraint(baseColumnNames: "student_majors_id", baseTableName: "student_major", constraintName: "FK_pufieujcutrsw3rdvf281oju3", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "student", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-64") {
		addForeignKeyConstraint(baseColumnNames: "minor_id", baseTableName: "student_minor", constraintName: "FK_ehda1ov56bmthfnxo71t0oc18", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "minor", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-65") {
		addForeignKeyConstraint(baseColumnNames: "student_minors_id", baseTableName: "student_minor", constraintName: "FK_7w9ekodl46n5f8d8exl3xptxn", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "student", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-66") {
		addForeignKeyConstraint(baseColumnNames: "organization_id", baseTableName: "student_organization", constraintName: "FK_g0l8g7giw62b35juc6u49n1th", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "organization", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-67") {
		addForeignKeyConstraint(baseColumnNames: "student_organizations_id", baseTableName: "student_organization", constraintName: "FK_lo5vqmhq6a5iuuvjfbvjkfycm", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "student", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-68") {
		addForeignKeyConstraint(baseColumnNames: "college_id", baseTableName: "university_college", constraintName: "FK_b31g06s6lqsa7vd9ooss35mm4", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "college", referencesUniqueColumn: "false")
	}

	changeSet(author: "brandon (generated)", id: "1428871464640-69") {
		addForeignKeyConstraint(baseColumnNames: "university_colleges_id", baseTableName: "university_college", constraintName: "FK_ghu4cs8gt7b531c1k453da810", deferrable: "false", initiallyDeferred: "false", referencedColumnNames: "id", referencedTableName: "university", referencesUniqueColumn: "false")
	}
}
