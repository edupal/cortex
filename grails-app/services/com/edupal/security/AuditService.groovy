package com.edupal.security

import com.edupal.AuditLog
import grails.transaction.Transactional

@Transactional
class AuditService {

    def springSecurityService

    def recordAudit(String controllerName, String actionName) {
        if(springSecurityService.isLoggedIn()){
            new AuditLog(user: SecUser.load(springSecurityService.principal.id), action: "${controllerName}:${actionName}").save()
        }
    }
}
