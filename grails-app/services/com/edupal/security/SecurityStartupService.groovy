package com.edupal.security

import com.edupal.Student
import grails.transaction.Transactional

@Transactional
class SecurityStartupService {

    def initData() {
        try {
            def adminRight = new SecRight(authority: 'ROLE_ADMIN').save();
            def studentRight = new SecRight(authority: 'ROLE_STUDENT').save();

            def adminGroup = new RightGroup(name: "Admin").save();
            def studentGroup = new RightGroup(name: "Student").save();

            RightGroupSecRight.create(adminGroup, adminRight)
            RightGroupSecRight.create(studentGroup, studentRight)


            def bUser = new SecUser(username: 'brandon@edupal.co', password: 'Password123');
            def tUser = new SecUser(username: 'tmh7sr@virginia.edu', password: 'Password123');
            def dUser = new SecUser(username: 'danfujita@danfujita.com', password: 'Password123');
            def student = new SecUser(username: 'test@edupal.co', password: 'Password123');
            bUser.save();
            tUser.save();
            dUser.save();
            student.save();

            SecUserRightGroup.create(bUser, adminGroup, true)
            SecUserRightGroup.create(tUser, adminGroup, true)
            SecUserRightGroup.create(dUser, adminGroup, true)
            SecUserRightGroup.create(student, studentGroup, true)

            def bStudent = new Student(name: "Brandon Wagner", user: bUser).save()
            def tStudent = new Student(name: "Tim Hammer", user: tUser).save()
            def dStudent = new Student(name: "Dan Fujita", user: dUser).save()
            def testStudent = new Student(name: "Bob Smith", user: student).save()
        }catch(Exception e){}




//        SecUserSecRight.create(bUser, adminRole);
//        SecUserSecRight.create(dUser, adminRole);
//        SecUserSecRight.create(dummyUser, userRole);


//        for (String url in [
//                '/', '/index', '/index.gsp', '/**/favicon.ico',
//                '/assets/**', '/**/js/**', '/**/css/**', '/**/images/**',
//                '**/login/**',
//                '**/logout/**']) {
//            new RequestMap(url: url, configAttribute: 'permitAll').save()
//        }
//        new RequestMap(url: '/j_spring_security_switch_user',
//                configAttribute: 'ROLE_SWITCH_USER,isFullyAuthenticated()').save()


    }

}
