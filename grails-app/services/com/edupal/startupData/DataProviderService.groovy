package com.edupal.startupData

import com.edupal.Attribute
import com.edupal.AttributeCategory
import com.edupal.AttributeValues
import com.edupal.College
import com.edupal.Course
import com.edupal.Department
import com.edupal.Major
import com.edupal.Minor
import com.edupal.Organization
import com.edupal.Student
import com.edupal.University
import grails.transaction.Transactional

@Transactional
class DataProviderService {

    def initData() {

        try {

            University gmu = new University(name: "George Mason University").save()
            University jmu = new University(name: "James Madison University").save()
            University gwu = new University(name: "George Washington University").save()
            University au = new University(name: "American University").save()
            University umbc = new University(name: "University of Maryland Baltimore County").save()

            College gmuEngineering = new College(name: "Volgenau School of Engineering")
            College gmuSob = new College(name: "School of Business")
            College gmuSol = new College(name: "School of Law")

            Department gmuCs = new Department(name: "Computer Science")
            Department gmuEE = new Department(name: "Electrical and Computer Engineering")
            Department gmuME = new Department(name: "Mechanical Engineering")

            Major cs = new Major(name: "Computer Science")
            Major acsswe = new Major(name: "Applied Computer Science Software Engineering")
            Major me = new Major(name: "Mechanical Engineering")

            Minor mincs = new Minor(name: "Computer Science")
            Minor minswe = new Minor(name: "Software Engineering")
            Minor minme = new Minor(name: "Mechanical Engineering")

            Course cs1 = new Course(name: "Introduction to Computer Science")
            Course cs2 = new Course(name: "Introduction to Python Programming")
            Course cs3 = new Course(name: "Introduction to Object Oriented Programming with Java")

            Course swe1 = new Course(name: "Introduction to Software Engineering")
            Course swe2 = new Course(name: "Introduction to Software Testing")
            Course swe3 = new Course(name: "Object Oriented Design and Architecture")


            gmu.addToColleges(gmuEngineering).save()
            gmu.addToColleges(gmuSob).save()
            gmu.addToColleges(gmuSol).save()

            gmuEngineering.addToDepartments(gmuCs).save()
            gmuEngineering.addToDepartments(gmuEE).save()
            gmuEngineering.addToDepartments(gmuME).save()

            gmuCs.addToMajors(cs).save()
            gmuCs.addToMajors(acsswe).save()
            gmuME.addToMajors(me).save()

            gmuCs.addToMinors(mincs).save()
            gmuCs.addToMinors(minswe).save()
            gmuME.addToMinors(minme).save()

            cs.addToCourses(cs1).save()
            cs.addToCourses(cs2).save()
            cs.addToCourses(cs3).save()

            acsswe.addToCourses(swe1).save()
            acsswe.addToCourses(swe2).save()
            acsswe.addToCourses(swe3).save()

            mincs.addToCourses(cs1).save()
            mincs.addToCourses(cs2).save()
            mincs.addToCourses(cs3).save()

            minswe.addToCourses(swe1).save()
            minswe.addToCourses(swe2).save()
            minswe.addToCourses(swe3).save()

            gmuCs.addToCourses(cs1).save()
            gmuCs.addToCourses(cs2).save()
            gmuCs.addToCourses(cs3).save()
            gmuCs.addToCourses(swe1).save()
            gmuCs.addToCourses(swe2).save()
            gmuCs.addToCourses(swe3).save()

            new Organization(name: "ACM", university: gmu).save(failOnError: true)
            new Organization(name: "IEEE", university: gmu).save(failOnError: true)
            new Organization(name: "Ethical Hackers", university: gmu).save(failOnError: true)

            new Organization(name: "ACM", university: jmu).save(failOnError: true)
            new Organization(name: "IEEE", university: jmu).save(failOnError: true)
            new Organization(name: "Ethical Hackers", university: jmu).save(failOnError: true)

            new Organization(name: "ACM", university: gwu).save(failOnError: true)
            new Organization(name: "IEEE", university: gwu).save(failOnError: true)

            new Organization(name: "ACM", university: au).save(failOnError: true)

            new Organization(name: "IEEE", university: umbc).save(failOnError: true)



            Attribute gpa = new Attribute(name: "GPA", weight: "1.0", category: AttributeCategory.Academic).save()
            AttributeValues attVal = new AttributeValues(attribute: gpa, value: "3.5").save()

            Student.findByNameIlike("Brandon Wagner").addToAttributes(attVal).save()



        }catch(Exception e){println e.getMessage()}

    }
}
