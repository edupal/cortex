package com.edupal

import com.edupal.security.*

class Student {

	String name
	SecUser user
	University university
	Collection majors
	Collection minors
	Collection organizations
	Collection courses
	Collection attributes
	BigDecimal probabilityOfSuccess
	Status status
	String bio

	static hasMany = [majors: Major, minors: Minor, organizations: Organization, courses: Course, attributes: AttributeValues, comments: Comment]

    static constraints = {
		status nullable: true
		university nullable: true
		probabilityOfSuccess nullable: true
		bio nullable: true, size: 3..250
		name nullable: false, size: 3..50
	}


	public String displayMajors(){
		return this.majors.toString().replace("]", "").replace("[", "")
	}
	public String displayMinors(){
		return this.minors.toString().replace("]", "").replace("[", "")
	}
	public String displayOrganizations(){
		return this.organizations.toString().replace("]", "").replace("[", "")
	}
	public String displayCourses(){
		return this.courses.toString().replace("]", "").replace("[", "")
	}
	public String displayAttributes(){
		return this.attributes.toString().replace("]", "").replace("[", "")
	}

	public String toString(){
		return name + (university ? (" from " + university) : "")
	}
}
