package com.edupal.security

import org.apache.commons.lang.builder.HashCodeBuilder

class SecUserRightGroup implements Serializable {

	private static final long serialVersionUID = 1

	SecUser secUser
	RightGroup rightGroup

	boolean equals(other) {
		if (!(other instanceof SecUserRightGroup)) {
			return false
		}

		other.secUser?.id == secUser?.id &&
		other.rightGroup?.id == rightGroup?.id
	}

	int hashCode() {
		def builder = new HashCodeBuilder()
		if (secUser) builder.append(secUser.id)
		if (rightGroup) builder.append(rightGroup.id)
		builder.toHashCode()
	}

	static SecUserRightGroup get(long secUserId, long rightGroupId) {
		SecUserRightGroup.where {
			secUser == SecUser.load(secUserId) &&
			rightGroup == RightGroup.load(rightGroupId)
		}.get()
	}

	static boolean exists(long secUserId, long rightGroupId) {
		SecUserRightGroup.where {
			secUser == SecUser.load(secUserId) &&
			rightGroup == RightGroup.load(rightGroupId)
		}.count() > 0
	}

	static SecUserRightGroup create(SecUser secUser, RightGroup rightGroup, boolean flush = false) {
		def instance = new SecUserRightGroup(secUser: secUser, rightGroup: rightGroup)
		instance.save(flush: flush, insert: true)
		instance
	}

	static boolean remove(SecUser u, RightGroup g, boolean flush = false) {
		if (u == null || g == null) return false

		int rowCount = SecUserRightGroup.where {
			secUser == SecUser.load(u.id) &&
			rightGroup == RightGroup.load(g.id)
		}.deleteAll()

		if (flush) { SecUserRightGroup.withSession { it.flush() } }

		rowCount > 0
	}

	static void removeAll(SecUser u, boolean flush = false) {
		if (u == null) return

		SecUserRightGroup.where {
			secUser == SecUser.load(u.id)
		}.deleteAll()

		if (flush) { SecUserRightGroup.withSession { it.flush() } }
	}

	static void removeAll(RightGroup g, boolean flush = false) {
		if (g == null) return

		SecUserRightGroup.where {
			rightGroup == RightGroup.load(g.id)
		}.deleteAll()

		if (flush) { SecUserRightGroup.withSession { it.flush() } }
	}

	static constraints = {
		secUser validator: { SecUser u, SecUserRightGroup ug ->
			if (ug.rightGroup == null) return
			boolean existing = false
			SecUserRightGroup.withNewSession {
				existing = SecUserRightGroup.exists(u.id, ug.rightGroup.id)
			}
			if (existing) {
				return 'userGroup.exists'
			}
		}
	}

	static mapping = {
		id composite: ['rightGroup', 'secUser']
		version false
	}
}
