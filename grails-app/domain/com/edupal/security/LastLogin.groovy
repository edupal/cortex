package com.edupal.security

class LastLogin {

    SecUser user
    Date lastLogin

    static constraints = {
        user nullable: false
        lastLogin nullable: false
    }

    public String toString(){
        return lastLogin.format("MM-dd-yyyy") + " at " + lastLogin.format("hh:mm")
    }
}
