package com.edupal.security

import org.apache.commons.lang.builder.HashCodeBuilder

class RightGroupSecRight implements Serializable {

	private static final long serialVersionUID = 1

	RightGroup rightGroup
	SecRight secRight

	boolean equals(other) {
		if (!(other instanceof RightGroupSecRight)) {
			return false
		}

		other.secRight?.id == secRight?.id &&
		other.rightGroup?.id == rightGroup?.id
	}

	int hashCode() {
		def builder = new HashCodeBuilder()
		if (rightGroup) builder.append(rightGroup.id)
		if (secRight) builder.append(secRight.id)
		builder.toHashCode()
	}

	static RightGroupSecRight get(long rightGroupId, long secRightId) {
		RightGroupSecRight.where {
			rightGroup == RightGroup.load(rightGroupId) &&
			secRight == SecRight.load(secRightId)
		}.get()
	}

	static boolean exists(long rightGroupId, long secRightId) {
		RightGroupSecRight.where {
			rightGroup == RightGroup.load(rightGroupId) &&
			secRight == SecRight.load(secRightId)
		}.count() > 0
	}

	static RightGroupSecRight create(RightGroup rightGroup, SecRight secRight, boolean flush = false) {
		def instance = new RightGroupSecRight(rightGroup: rightGroup, secRight: secRight)
		instance.save(flush: flush, insert: true)
		instance
	}

	static boolean remove(RightGroup rg, SecRight r, boolean flush = false) {
		if (rg == null || r == null) return false

		int rowCount = RightGroupSecRight.where {
			rightGroup == RightGroup.load(rg.id) &&
			secRight == SecRight.load(r.id)
		}.deleteAll()

		if (flush) { RightGroupSecRight.withSession { it.flush() } }

		rowCount > 0
	}

	static void removeAll(SecRight r, boolean flush = false) {
		if (r == null) return

		RightGroupSecRight.where {
			secRight == SecRight.load(r.id)
		}.deleteAll()

		if (flush) { RightGroupSecRight.withSession { it.flush() } }
	}

	static void removeAll(RightGroup rg, boolean flush = false) {
		if (rg == null) return

		RightGroupSecRight.where {
			rightGroup == RightGroup.load(rg.id)
		}.deleteAll()

		if (flush) { RightGroupSecRight.withSession { it.flush() } }
	}

	static constraints = {
		secRight validator: { SecRight r, RightGroupSecRight rg ->
			if (rg.rightGroup == null) return
			boolean existing = false
			RightGroupSecRight.withNewSession {
				existing = RightGroupSecRight.exists(rg.rightGroup.id, r.id)
			}
			if (existing) {
				return 'roleGroup.exists'
			}
		}
	}

	static mapping = {
		id composite: ['rightGroup', 'secRight']
		version false
	}
}
