package com.edupal.security

class RightGroup {

	String name

	static mapping = {
		cache true
    }

	Set<SecRight> getAuthorities() {
		RightGroupSecRight.findAllByRightGroup(this).collect { it.secRight }
	}

	static constraints = {
		name matches: "[a-zA-Z-' ]+", blank: true, unique: true

	}
}
