package com.edupal.security

import com.edupal.Student

class SecUser {

	transient springSecurityService

	String username
	String password
	boolean enabled = true
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired

	static transients = ['springSecurityService']

	static constraints = {
		username blank: false, unique: true, size: 3..64;
//		password blank: false, size: 7..64, matches: "(.*[A-Z]+.*[1-9]+)| (.*[1-9]+.*[A-Z]+)"
	}

	static mapping = {
		password column: '`password`'
	}

	Set<RightGroup> getAuthorities() {
		SecUserRightGroup.findAllBySecUser(this).collect { it.rightGroup }
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService?.passwordEncoder ? springSecurityService.encodePassword(password) : password
	}

	public String toString(){
		return Student.findByUser(this)?.name
	}
}
