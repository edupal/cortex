package com.edupal.security

import org.apache.commons.lang.builder.HashCodeBuilder

class SecUserSecRight implements Serializable {

	private static final long serialVersionUID = 1

	SecUser secUser
	SecRight secRight

	boolean equals(other) {
		if (!(other instanceof SecUserSecRight)) {
			return false
		}

		other.secUser?.id == secUser?.id &&
		other.secRight?.id == secRight?.id
	}

	int hashCode() {
		def builder = new HashCodeBuilder()
		if (secUser) builder.append(secUser.id)
		if (secRight) builder.append(secRight.id)
		builder.toHashCode()
	}

	static SecUserSecRight get(long secUserId, long secRightId) {
		SecUserSecRight.where {
			secUser == SecUser.load(secUserId) &&
			secRight == SecRight.load(secRightId)
		}.get()
	}

	static boolean exists(long secUserId, long secRightId) {
		SecUserSecRight.where {
			secUser == SecUser.load(secUserId) &&
			secRight == SecRight.load(secRightId)
		}.count() > 0
	}

	static SecUserSecRight create(SecUser secUser, SecRight secRight, boolean flush = false) {
		def instance = new SecUserSecRight(secUser: secUser, secRight: secRight)
		instance.save(flush: flush, insert: true)
		instance
	}

	static boolean remove(SecUser u, SecRight r, boolean flush = false) {
		if (u == null || r == null) return false

		int rowCount = SecUserSecRight.where {
			secUser == SecUser.load(u.id) &&
			secRight == SecRight.load(r.id)
		}.deleteAll()

		if (flush) { SecUserSecRight.withSession { it.flush() } }

		rowCount > 0
	}

	static void removeAll(SecUser u, boolean flush = false) {
		if (u == null) return

		SecUserSecRight.where {
			secUser == SecUser.load(u.id)
		}.deleteAll()

		if (flush) { SecUserSecRight.withSession { it.flush() } }
	}

	static void removeAll(SecRight r, boolean flush = false) {
		if (r == null) return

		SecUserSecRight.where {
			secRight == SecRight.load(r.id)
		}.deleteAll()

		if (flush) { SecUserSecRight.withSession { it.flush() } }
	}

	static constraints = {
		secRight validator: { SecRight r, SecUserSecRight ur ->
			if (ur.secUser == null) return
			boolean existing = false
			SecUserSecRight.withNewSession {
				existing = SecUserSecRight.exists(ur.secUser.id, r.id)
			}
			if (existing) {
				return 'userRole.exists'
			}
		}
	}

	static mapping = {
		id composite: ['secRight', 'secUser']
		version false
	}
}
