package com.edupal

class Attribute {

    String name
    BigDecimal weight
    AttributeCategory category

    static constraints = {
        name matches: "^[a-zA-Z-' ]+\$"
        weight validator:{
            return it>0 && it<=100
        }



    }
}
