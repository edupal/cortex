package com.edupal

class Minor {

    String name
    Collection courses

    static hasMany = [courses: Course]

    static constraints = {
        name matches: "[a-zA-Z-' ]+"
    }

    public String toString(){
        return name
    }
}
