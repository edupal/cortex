package com.edupal

import com.edupal.security.SecUser

class AuditLog {

    SecUser user
    String action
    Date actionTime = new Date()

    static constraints = {
        user nullable: false
        action nullable: false
        actionTime nullable: false
    }

    public String toString(){
        return "${user.username} performed action: ${action} at ${actionTime}"
    }
}
