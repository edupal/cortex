package com.edupal

class Department {

    String name
    Collection majors
    Collection minors
    Collection courses

    static hasMany = [majors: Major, minors: Minor, courses: Course]

    static belongsTo = College

    static constraints = {
        name matches: "[a-zA-Z-' ]+"
    }
}
