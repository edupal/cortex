package com.edupal

class Major {

    String name
    Collection courses

    static hasMany = [courses: Course]

    static constraints = {
        name matches: "[a-zA-Z-' ]+"

    }

    public String toString(){
        return name
    }
}
