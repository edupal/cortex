package com.edupal

class AttributeValues {

    String value

    Attribute attribute

    static constraints ={
            value nullable:false;
    }
}
