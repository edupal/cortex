package com.edupal

class Organization {

    String name

    University university

    static hasOne = University

    static constraints = {
        name matches: "[a-zA-Z-' ]+"

    }

    public String toString(){
        return name
    }
}
