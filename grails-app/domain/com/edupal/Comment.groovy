package com.edupal

import com.edupal.security.SecUser

class Comment {

    SecUser user
    String comment
    Date commentDate = new Date()

    static constraints = {
        user nullable: false
        comment nullable: false, size: 3..250
        commentDate nullable: false
    }

    public String toString(){
        return comment
    }
}
