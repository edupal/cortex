package com.edupal

import com.edupal.security.SecUser

class Post {

    SecUser user
    String post
    Date postDate = new Date()

    static constraints = {
        user nullable: false
        post nullable: false
        postDate nullable: false
    }

    public String toString(){
        return post
    }

    public static getPosts(SecUser user){

        def users = Student.findAllByUniversity(Student.findByUser(user)?.university)?.user
        def posts = []
        users.each {
            posts << Post.findAllByUser(it)
        }
        posts.sort{it.postDate}.flatten().reverse()
    }
}
