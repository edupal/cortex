package com.edupal

class College {

    String name
    Collection departments

    static hasMany = [departments: Department]

    static belongsTo = University

    static constraints = {
        name matches: "[a-zA-Z-' ]+"
    }
}
