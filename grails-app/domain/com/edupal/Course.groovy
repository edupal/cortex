package com.edupal

class Course {

    String name

    static belongsTo = Department

    static constraints = {
        name matches: "[a-zA-Z-' ]+"

    }


    public String toString(){
        return name
    }
}
