package com.edupal

class University {

    String name
    Collection colleges

    static hasMany = [colleges: College]

    static constraints = {
        name matches: "[a-zA-Z-' ]+"

    }

    public String toString(){
        return name
    }
}
