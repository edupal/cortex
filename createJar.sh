#!/usr/bin/env bash

grails dev build-standalone cortex.jar -Dbuild.compiler=javac1.7
java -jar cortex.jar keystorePath=./ssl/keystore.jks keystorePassword=isa681 port=8080 httpsPort=8443

