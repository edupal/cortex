package com.edupal

import grails.test.mixin.TestMixin
import grails.test.mixin.support.GrailsUnitTestMixin
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestMixin(GrailsUnitTestMixin)
@Mock([Attribute])
class AttributeSpec extends Specification {

    def setup() {

    }

    def cleanup() {
    }

    void "Name:Just Name"() {
        given:
        def test1 = new Attribute(name:'dan',weight:2.5,category:AttributeCategory.Academic);

        expect:
        test1.validate() == true
        test1.errors.allErrors.size() == 0
    }
    void "Name:Numbers"() {
        given:
        def test2 = new Attribute(name:'dan12345',weight:2.5,category:AttributeCategory.Academic);
        expect:
        test2?.validate() == false
        test2.errors.allErrors.size() != 0

    }
    void "Name:Special Characters"() {
        given:
        def test3 = new Attribute(name:'dan!$#$#wre',weight:2.5,category:AttributeCategory.Academic);
        expect:
        test3?.validate() == false
        test3.errors.allErrors.size() != 0

    }
    void "Name:Include Hyphen "() {
        given:
        def test4 = new Attribute(name:'dan-fujita',weight:2.5,category:AttributeCategory.Academic);
        expect:

        test4.validate()==true;
        test4.errors.allErrors.size() == 0

    }
    void "Include Bracket "() {
        given:
        def test5 = new Attribute(name:'dan;fujita',weight:2.5,category:AttributeCategory.Academic);
        expect:

        test5?.validate()==false;
        test5.errors.allErrors.size() != 0

    }


    void "Weight:More than 100"() {
        given:
        def test6 = new Attribute(name:'dan',weight:101,category:AttributeCategory.Academic);
        expect:

        test6?.validate()==false;
        test6.errors.allErrors.size() != 0

    }
    void "Weight:Negative"() {
        given:
        def test7 = new Attribute(name:'dan',weight:-1,category:AttributeCategory.Academic);
          expect:

          test7?.validate()==false;
          test7.errors.allErrors.size() != 0

    }
    void "Weight:Correct Number"() {
        given:
        def test8 = new Attribute(name:'dan',weight:15,category:AttributeCategory.Academic);
        expect:

        test8.validate()==true;
        test8.errors.allErrors.size() == 0

    }

}
