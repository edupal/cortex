package com.edupal

import com.edupal.security.SecUser
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Student)
@Mock([Student,University,SecUser])

class StudentSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "Name:Just Name"() {
        given:
        def test1 = new University(name:'dan',user:new SecUser(),university:new University());

        expect:
        test1.validate() == true
        test1.errors.allErrors.size() == 0
    }
    void "Name:Numbers"() {
        given:
        def test2 = new University(name:'dan12345',user:new SecUser(),university:new University());
        expect:
        test2?.validate() == false
        test2.errors.allErrors.size() != 0

    }
    void "Name:Special Characters"() {
        given:
        def test3 = new University(name:'dan!$#$#wre',user:new SecUser(),university:new University());
        expect:
        test3?.validate() == false
        test3.errors.allErrors.size() != 0

    }
    void "Name:Include Hyphen "() {
        given:
        def test4 = new University(name:'dan-fujita',user:new SecUser(),university:new University());
        expect:

        test4.validate()==true;
        test4.errors.allErrors.size() == 0

    }
    void "Name:Dan{Fujita"() {
        given:
        def test5 = new University(name:'dan{fujita',user:new SecUser(),university:new University());
        expect:

        test5?.validate()==false;
        test5.errors.allErrors.size() != 0

    }
    void "Name:False"() {
        given:
        def test6 = new University(name:null,user:new SecUser(),university:new University());
        expect:

        test6?.validate()==false;
        test6.errors.allErrors.size() != 0

    }
    void "Just Hyphen"() {
        given:
        def test7 = new University(name:'-',user:new SecUser(),university:new University());
        expect:

        test7.validate()==true;
        test7.errors.allErrors.size() == 0

    }
}
