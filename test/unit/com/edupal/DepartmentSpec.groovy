package com.edupal

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Department)
@Mock([Department])

class DepartmentSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "Name:Just Name"() {
        given:
        def test1 = new Department(name:'dan');

        expect:
        test1.validate() == true
        test1.errors.allErrors.size() == 0
    }
    void "Name:Numbers"() {
        given:
        def test2 = new Department(name:'dan12345');
        expect:
        test2?.validate() == false
        test2.errors.allErrors.size() != 0

    }
    void "Name:Special Characters"() {
        given:
        def test3 = new Department(name:'dan!$#$#wre');
        expect:
        test3?.validate() == false
        test3.errors.allErrors.size() != 0

    }
    void "Name:Include Hyphen "() {
        given:
        def test4 = new Department(name:'dan-fujita');
        expect:

        test4.validate()==true;
        test4.errors.allErrors.size() == 0

    }
    void "Name:Dan{Fujita"() {
        given:
        def test5 = new Department(name:'dan{fujita');
        expect:

        test5?.validate()==false;
        test5.errors.allErrors.size() != 0

    }
    void "Name:False"() {
        given:
        def test6 = new Department(name:null);
        expect:

        test6?.validate()==false;
        test6.errors.allErrors.size() != 0

    }
    void "Just Hyphen"() {
        given:
        def test7 = new Department(name:'-');
        expect:

        test7.validate()==true;
        test7.errors.allErrors.size() == 0

    }
}
